CPSQuickStart                                             10.04.2018 - florian0
===============================================================================

This tool acts as an addon to sro_client.exe. It overrides various functions of
the client and is therefore not compatible with any other 3rd-party software
and/or might even break other 3rd-party software.

It enables a hidden feature called QuickStart which skips the whole login and 
performs an automated login. 

-------------------------------------------------------------------------------
Usage:

Copy the Dll to your Silkroad Folder, run the client and inject the dll. The 
DllMain must be executed before the original WinMain of the Clients starts. 
Your injection method can be either detouring the entrypoint or adding an 
import. Common dll-injection-tools may work if they support creating the client
process suspended.

-------------------------------------------------------------------------------
Bugs:

* Wrong credentials, wrong character name, wrong server name, wrong image code
will possibly lead to a crash. Be aware of that.
* The IBUV can not be displayed. You'll either need to pseudo-disable (length
to 0) it or guess it (good luck).


-------------------------------------------------------------------------------
Compiling:

As always, compiling is a little tricky. This software is designed to be binary
compatible to the client. Therefore building with Visual C++ 8.0 (install 
Visual Studio 2005) is mandatory! You'll need Daffodil (1) and Visual Studio 
2010 to open the project. You'll also need DirectX SDK (i'm using v9.0b, but 
newers might work, too). Put it inside the lib-folder.

Always compile on Release-mode. Debug will produce incompatible code.

This combination is guaranteed to work as it is binary compatible. Newer 
compilers can work, but are neither tested nor supported by me.

(1) https://daffodil.codeplex.com/

Compatibility in Visual Studio:

Visual Studio 2005 | YES
Visual Studio 2008 | <untested>
Visual Studio 2010 | YES
Visual Studio 2012 | <untested>
Visual Studio 2013 | <untested>
Visual Studio 2015 | <untested>
Visual Studio 2017 | <untested>

-------------------------------------------------------------------------------
Attribution:

Do what ever the f*ck you want with it. But don't be that a**hole claiming 
others work as your own. Thank you.
