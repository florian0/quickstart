#include <Windows.h>
#include <cstdio>

#include "hook.h"
#include "Client.h"

#include "sdk/gfx/GFX3DFunction/GFXVideo3d.h"
#include "engine/process/PSQuickStart.h"

HINSTANCE dllInstance;
HMODULE sro_client = 0;

// Enter your credentials here
wchar_t *servername = L"Testserver";
wchar_t *userid = L"HansYolo";
wchar_t *passwd = L"Abc123";
char* charname = "florian0";
char* ibuv_text = ""; // IBUV = Image Code


BOOL WINAPI DllMain(HINSTANCE hModule, DWORD fdwReason, LPVOID lpReserved) {
	
	if (fdwReason == DLL_PROCESS_ATTACH) {
		replaceOffset(0x00B49AE4, (int)&_FakeWinMain);

		replaceAddr(0x00E096A4, addr_from_this(&CGFXVideo3d::EndSceneIMPL));

		// Override credentials & info
		replaceAddr(0x00EC2444, reinterpret_cast<int>(servername));
		replaceAddr(0x00EC2448, reinterpret_cast<int>(userid));
		replaceAddr(0x00EC244C, reinterpret_cast<int>(passwd));

		
		replaceAddr(0x00DD868C, addr_from_this(&CPSQuickStart::OnCreate_IMPL));
		replaceAddr(0x00DD867C, addr_from_this(&CPSQuickStart::OnPacket_MAYBE_IMPL));


		AllocConsole();
		freopen("CONOUT$", "w", stdout);
		freopen("CONIN$", "r", stdin);

		printf("florian0's dev-client\n");
		
		dllInstance = hModule;
		printf("hModule = 0x%08x\n", dllInstance);
	} else if (fdwReason == DLL_PROCESS_DETACH) {
		
	}

	return TRUE;
}
