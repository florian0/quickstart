#pragma once

#include "GfxRuntimeClass.h"

class CObj {
public:
	virtual CGfxRuntimeClass *GetRuntimeClass();
	virtual CGfxRuntimeClass *GetParentType();
	virtual ~CObj();
};
