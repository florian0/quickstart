#pragma once

class CObj;
struct CGfxRuntimeClass;

#include "Obj.h"
#include <Windows.h>

// Funfact: Joymax stole this class from afx.h and renamed various macros. LOL.
struct CGfxRuntimeClass
{
	const char * m_lpszClassName;
	int m_nObjectSize;
	unsigned int m_wSchema; // schema number of the loaded class

	CGfxRuntimeClass *m_pBaseClass;

	int field_10;
	int field_14;

	CObj*(__stdcall *m_pfnCreateObject)();
	CObj*(__stdcall *m_pfnDeleteObject)();



	CObj *CreateObject();
	BOOL IsDerivedFrom(const CGfxRuntimeClass* pBaseClass) const;


	// dynamic name lookup and creation
	static CGfxRuntimeClass* __stdcall FromName(const char * lpszClassName);
	static CGfxRuntimeClass* __stdcall FromName(const wchar_t * lpszClassName);
	static CObj* __stdcall CreateObject(const char * lpszClassName);
	static CObj* __stdcall CreateObject(const wchar_t * lpszClassName);
};

