#pragma once
#include <string>
#include "LocalTime.h"

class CClientConfig
{
public:

	static CClientConfig* get()
	{
		return reinterpret_cast<CClientConfig*>(0x00EED578);
	}

	char pad_0x0000[0x50]; //0x0000
	std::string str_loginserver; //0x0050 
	std::string str_realm; //0x006C 
	char pad_0x0088[0x4]; //0x0088
	std::string str_regkey; //0x008C 
	std::string str_serverdep; //0x00A8 
	std::string str_sometimestamp; //0x00C4 way back in the past
	char pad_0x00E0[0x18]; //0x00E0
	CLocalTime N00000FFA; //0x00F8 
	char pad_0x0114[0x1B]; //0x0114
	__int8 config_debug_mode; //0x012F @12F 
	char pad_0x0130[0x30]; //0x0130
	__int32 N000013F7; //0x0160 compared to 1 in CPSLogo::OnCreate
	char pad_0x0164[0x20]; //0x0164
	std::string str_emblemserver; //0x0184 
	std::string str_emblempath; //0x01A0 
	std::string str_intro_script; //0x01BC 
	std::string str_intro_bgm; //0x01D8 
};

