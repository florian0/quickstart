CPSQuickStart SOURCECODE                                  10.04.2018 - florian0
===============================================================================

This file acts as a small introduction on my code. I've tried to fake the 
original source layout to get a better understanding how Silkroad Online was
developed and maintained. The implementation is designed to binary compatible
to the original client. This allows using the std-lib without any wrappers and
makes your life a lot easier.

-------------------------------------------------------------------------------
Where to start?

src/engine/process/PSQuickStart.cpp:OnPacket_MAYBE_IMPL contains the most 
important part. It catches away the following MsgIds from the original handler:
* 0x1002 (IBUV_REQUIRED)
* 0x1003 (IBUV_RESPONSE)
* 0xB007(CHARACTER_SELECT_REQUIRED)
All other MsgIds get passed to the original handler.

I also decided to override CPSQuickStart::OnCreate because it could not handle 
resolutions other than 1024x768 (fixed by calling CGame::ResizeMainWindow()).

The DllMain.cpp contains all credentials and also the hooking.

Client.cpp::FakeWinMain will set the StartProcess to CPSQuickStart (around 
line 100). I put it right after the initialization of the framework but before
the run command.

-------------------------------------------------------------------------------
How does it work at all?

The DllMain overwrites the WinMain with out FakeWinMain(Client.cpp) and the 
EndScene-Function of CGFXVideo3d (sdk/gfx/GFX3DFunction/GFXVideo3d.cpp).

These two hooks allow access to the 3d renderer for imgui windows, and the 
main loop to get mouse movement and keystrokes.

The client will startup as usual, but run on our custom WinMain instead.

FakeWinMain will setup imgui and supply it with messages. It will also set 
CPSQuickStart as StartProcess.

CPSQuickStart::OnCreate will initialize the network (NetworkConnect) and then 
handle all the messages in CPSQuickStart::OnPacket_MAYBE. It does only handle 
MsgIds that were either unsupported or bugged due to changes to the network. 
All working Msgs will be forwarded to the original handler function.

-------------------------------------------------------------------------------
Attribution:

Do what ever the f*ck you want with it. But don't be that a**hole claiming 
others work as your own. Thank you.
